;;; Guile-OpenGL
;;; Copyright © 2023 David Thompson <davet@gnu.org>
;;;
;;; Guile-OpenGL is free software: you can redistribute it and/or modify
;;; it under the terms of the GNU Lesser General Public License as
;;; published by the Free Software Foundation, either version 3 of the
;;; License, or (at your option) any later version.
;;;
;;; Guile-OpenGL is distributed in the hope that it will be useful, but
;;; WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
;;; Lesser General Public License for more details.
;;;
;;; You should have received a copy of the GNU Lesser General Public
;;; License along with this program.  If not, see
;;; <http://www.gnu.org/licenses/>.

;;; Commentary:
;;
;; Development environment for GNU Guix.
;;
;; To setup the development environment and build, run the following:
;;
;;    guix shell
;;    ./bootstrap
;;    ./configure
;;    make
;;
;; To build the development snapshot, run:
;;
;;    guix build -f guix.scm
;;
;; To install the development snapshot, run:
;;
;;    guix install -f guix.scm
;;
;;; Code:

(use-modules (guix packages)
             ((guix licenses) #:prefix license:)
             (guix git)
             (guix build-system gnu)
             (gnu packages)
             (gnu packages autotools)
             (gnu packages pkg-config)
             (gnu packages texinfo)
             (gnu packages guile)
             (gnu packages gl))

(package
  (name "guile-opengl")
  (version "0.2.0-git")
  (source (git-checkout (url (dirname (current-filename)))))
  (build-system gnu-build-system)
  (native-inputs (list autoconf automake pkg-config texinfo))
  (inputs (list guile-3.0 mesa glu freeglut))
  (arguments
   '(#:make-flags '("GUILE_AUTO_COMPILE=0")
     #:phases (modify-phases %standard-phases
                (add-before 'build 'patch-dynamic-link
                  (lambda* (#:key inputs outputs #:allow-other-keys)
                    (substitute* "gl/runtime.scm"
                      (("\\(dynamic-link\\)")
                       (string-append "(dynamic-link \""
                                      (assoc-ref inputs "mesa")
                                      "/lib/libGL.so" "\")")))
                    (define (dynamic-link-substitute file lib input)
                      (substitute* file
                        (("dynamic-link \"lib([a-zA-Z]+)\"" _ lib)
                         (string-append "dynamic-link \""
                                        (assoc-ref inputs input)
                                        "/lib/lib" lib "\""))))
                    ;; Replace dynamic-link calls for libGL, libGLU, and
                    ;; libglut with absolute paths to the store.
                    (dynamic-link-substitute "glx/runtime.scm" "GL" "mesa")
                    (dynamic-link-substitute "glu/runtime.scm" "GLU" "glu")
                    (dynamic-link-substitute "glut/runtime.scm" "glut"
                                             "freeglut"))))))
  (home-page "https://gnu.org/s/guile-opengl")
  (synopsis "Guile binding for the OpenGL graphics API")
  (description
   "Guile-OpenGL is a library for Guile that provides bindings to the
OpenGL graphics API.")
  (license license:lgpl3+))
